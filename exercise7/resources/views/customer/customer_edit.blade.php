<form method="post" action="/customer/update/{{ $customer->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" value=" {{ $customer->nama }}">
                            <label>Alamat</label>
                            <textarea name="alamat"> {{ $customer->alamat }} </textarea>
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>